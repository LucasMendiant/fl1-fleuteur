import 'package:flutter/material.dart';
import 'package:multimedia_app/authentification/error_handler.dart';
import 'package:multimedia_app/authentification/login.dart';

import 'package:multimedia_app/services/authservices.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  
  final _formKey = GlobalKey<FormState>();
  String email, password, confirmpassword;
  checkFields() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    else {
      return false;
    }
  }
  String validationEmail(String email) {
    if (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email)) {
      return 'Veuillez entrer un email valide';
    } else {
      return null;
    }
  }
  String validationPassword(String email) {
    if (confirmpassword != password) {
      return 'Les mots de passe ne correspondent pas !';
    } else {
      return null;
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.blue,
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.only(left: 25, right:25),
            child: ListView(
            children: [
              const SizedBox(height: 50),
              SizedBox(
              height: 125.0,
              width: 200.0,
              child: Stack(
                children: const [
                  Text('MyFlueut',
                      style: TextStyle(
                        fontFamily: 'Montserrat',
                        fontSize: 60.0,
                        color: Colors.white,
                  )),
                ],
              )),
              Column(
                  children: <Widget>[
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Email',
                        labelStyle: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.pink
                          )
                        )
                      ),
                      onChanged: (String value) {
                        email = value;
                      },
                      validator: (String value) =>
                        value.isEmpty ? "Champ obligatoire" : validationEmail(value)),
                  ],
                ),
              const SizedBox(height: 5),
              Column(
                  children: <Widget>[
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Mot de passe',
                        labelStyle: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.pink
                          )
                        )),
                      obscureText: true,
                      onChanged: (String value) {
                        password = value;
                      },
                      validator: (String value) =>
                        value.isEmpty ? 'Champ obligatoire' : null,
                    ),
                  ],
                ),
              const SizedBox(height: 5),
              Column(
                  children: <Widget>[
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Confirmer le mot de passe',
                        labelStyle: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.pink
                          )
                        )),
                      obscureText: true,
                      onChanged: (String value) {
                        confirmpassword = value;
                      },
                      validator: (String value) =>
                        value.isEmpty ? 'Champ obligatoire' : validationPassword(value),
                    ),
                  ],
              ),
              const SizedBox(height: 35),
              GestureDetector(
                onTap: () {
                      if(checkFields()) {
                        AuthService().signUp(email, password).then((userCreds) {
                          Navigator.of(context).pop();
                        }).catchError((e) {
                          ErrorHandler().errorDialog(context, e);
                        });
                      }
                    },
                child:
                SizedBox(
                  height: 50,
                  child: Material(
                    borderRadius: BorderRadius.circular(20),
                    shadowColor: Colors.pinkAccent,
                    color: Colors.pink,
                    elevation: 7.0,
                    child: const Center(
                      child: Text(
                        "S'enregistrer",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat',
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => const LoginPage()));
                  },
                  child: const Text("Dejà enregistré ?",
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.underline
                  ),
                  ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      ),
    );
  }
}
