import 'dart:io';
import 'package:multimedia_app/widgets/bottom-sheet-camera.dart';
import 'package:multimedia_app/widgets/list-tiles/simple-listTile-profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_screenutil/screenutil_init.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multimedia_app/widgets/list-tiles/list-tiles-profile.dart';
import 'package:multimedia_app/widgets/list-tiles/list-tiles-socialNetworks.dart';
import 'package:multimedia_app/widgets/nav-drawer.dart';
import 'edit-profile.dart';
import 'package:provider/provider.dart';
import 'package:multimedia_app/controller/userController.dart';

class InitScreen extends StatefulWidget {
  @override
  _InitScreenState createState() => _InitScreenState();
}

class _InitScreenState extends State<InitScreen> {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(410, 730),
      allowFontScaling: false,
      builder: () => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter_ScreenUtil',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Profile(),
      ),
    );
  }
}

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build (BuildContext context) {
    UserController userController = Provider.of<UserController>(context);
    userController.initializerAllVariable();
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text('Profile page'),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.lightBlue, Colors.indigoAccent],
                )
              ),
              width: MediaQuery.of(context).size.width,
              height: ScreenUtil().setHeight(730/3),
              child: Stack(
                children: [
                  Center(
                    child: FutureBuilder(
                      future: userController.storage.ready,
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (snapshot.data == null) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        return CircleAvatar(
                            backgroundImage: userController.imageUrl == null ? AssetImage("assets/profile_picture.jpg") : FileImage(File(userController.imageUrl)),
                            radius: 100.0
                        );
                      },
                    ),
                  ),
                  Container(
                    alignment: Alignment.bottomRight,
                    child: IconButton(
                      icon: Icon(
                        Icons.edit,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        showModalBottomSheet(
                            context: context,
                            builder: ((builder) => BottomSheetProfile(callbackCam: () => userController.takePicture(ImageSource.camera), callbackGal: () => userController.takePicture(ImageSource.gallery)))
                        );
                      },
                    ),
                  )
                ],
              ),
            ),

            Center(
              child: Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ListTile(
                        title: Text(
                        userController.email,
                        style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blueAccent
                          ),
                        )
                    ),
                  ],
                ),
              ),
            ),


            Center(
              child: Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        icon: Icon(
                          Icons.edit,
                        ),
                        onPressed: () async {
                          Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => EditProfile())
                          );
                        },
                      ),
                    ),
                    FutureBuilder(
                      future: userController.storage.ready,
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        return SimpleTile();
                      }
                    ),
                  ],
                ),
              ),
            ),
            Center(
              child: Card(
                child: FutureBuilder(
                  future: userController.storage.ready,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    return ProfileTiles();
                  },
                ),
              ),
            ),
            Center(
              child: Card(
                child: FutureBuilder(
                  future: userController.storage.ready,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    return SocialNetworksTiles();
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}