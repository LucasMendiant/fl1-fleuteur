import 'package:multimedia_app/widgets/dropdownbutton-country-profile.dart';
import 'package:multimedia_app/widgets/input-text-column-edit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screen_util.dart';
import 'package:provider/provider.dart';
import 'package:multimedia_app/controller/userController.dart';


class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  @override
  Widget build(BuildContext context) {
    UserController userController = Provider.of<UserController>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Edition de profile'),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InputTextEditProfile(controllers: userController.bioController, hintTitle: 'changer votre bio', maxLines: 8, bottomValue: 135.0, iconC: Icons.account_circle),
            InputTextEditProfile(controllers: userController.ageController, hintTitle: 'Changer votre age', maxLines: 1, bottomValue: 0.0, iconC: Icons.assignment_ind, ktype: TextInputType.number),
            InputTextEditProfile(controllers: userController.townController, hintTitle: 'changer votre localisation', maxLines: 1, bottomValue: 0.0, iconC: Icons.add_location_alt_rounded),
            DropDownButtonCountry(countries: userController.countriesList,  currentItemSelected: 'Changer votre pays', country: '', countryCallback: userController.refresh),
            InputTextEditProfile(controllers: userController.facebookUrlController, hintTitle: 'Changer votre url Facebook', maxLines: 1, bottomValue: 0.0, iconC: Icons.chat, ktype: TextInputType.url),
            InputTextEditProfile(controllers: userController.instaUrlController, hintTitle: 'Changer votre url Instagram', maxLines: 1, bottomValue: 0.0, iconC: Icons.favorite, ktype: TextInputType.url),
            InputTextEditProfile(controllers: userController.twitterUrlController, hintTitle: 'Changer votre url Twitter', maxLines: 1, bottomValue: 0.0, iconC: Icons.contactless_rounded, ktype: TextInputType.url),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 50),
                  child: SizedBox(
                    width: ScreenUtil().setWidth(100),
                    child: OutlinedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        'Cancel',
                        style: TextStyle(fontSize: 16, color: Colors.black),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 50),
                  child: SizedBox(
                    width: ScreenUtil().setWidth(100),
                    child: ElevatedButton(
                      onPressed: () {
                        if (userController.bioController.text != null && userController.bioController.text != '') {
                          userController.setBio(userController.bioController.text);
                        }
                        if (userController.ageController.text != null && userController.ageController.text != '') {
                          userController.setAge(userController.ageController.text);
                        }
                        if (userController.townController.text != null && userController.townController.text != '') {
                          userController.setTown(userController.townController.text);
                        }
                        if (userController.countrySelected != null && userController.countrySelected != '') {
                          userController.setCountry(userController.countrySelected);
                        }
                        if (userController.facebookUrlController.text != null && userController.facebookUrlController.text != '') {
                          userController.setFacebookUrl(userController.facebookUrlController.text);
                        }
                        if (userController.instaUrlController.text != null && userController.instaUrlController.text != '') {
                          userController.setInstaUrl(userController.instaUrlController.text);
                        }
                        if (userController.twitterUrlController.text != null && userController.twitterUrlController.text != '') {
                          userController.setTwitterUrl(userController.twitterUrlController.text);
                        }
                        Navigator.pop(context);
                      },
                      child: Text(
                        'Save',
                        style: TextStyle(fontSize: 16, color: Colors.white),
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}