import 'package:flutter/material.dart';
import 'dart:io';
import 'package:provider/provider.dart';
import 'package:multimedia_app/controller/musicController.dart';

// ignore: must_be_immutable
class TrackListener extends StatefulWidget {
  TrackListenerState createState() => TrackListenerState();
}

class TrackListenerState extends State<TrackListener> {
  
  Widget build(context) {
    MusicController musicController = Provider.of<MusicController>(context);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightBlue[600],
          leading: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
                musicController.stop();
              },
              icon: Icon(
                Icons.arrow_back_ios_sharp,
              )),
          title: Text('Lecture en'),
        ),
        body: Container(
          margin: EdgeInsets.fromLTRB(5, 25, 5, 0),
          child: SingleChildScrollView(
            child: Column(children: <Widget>[
              CircleAvatar(
                backgroundImage: musicController.songInfo.albumArtwork == null
                    ? AssetImage('assets/music_background.jpg')
                    : FileImage(File(musicController.songInfo.albumArtwork)),
                radius: 75,
              ),
              Container(
                child: Text(
                  musicController.songInfo.title,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14.0,
                      fontWeight: FontWeight.w600),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 16),
                child: Text(
                  musicController.songInfo.artist,
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500),
                ),
              ),
              Slider(
                inactiveColor: Colors.lightBlue[300],
                activeColor: Colors.lightBlue[800],
                min: musicController.minimumValue,
                max: musicController.maximumValue,
                value: musicController.currentValue,
                onChanged: (value) {
                  musicController.setCurrentValue(value);
                  musicController.player.seek(Duration(milliseconds: musicController.currentValue.round()));
                },
              ),
              Container(
                transform: Matrix4.translationValues(0, -15, 0),
                margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(musicController.currentTime,
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 12.5,
                            fontWeight: FontWeight.w500)),
                    Text(musicController.endTime,
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 12.5,
                            fontWeight: FontWeight.w500))
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(10, 25, 5, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    GestureDetector(
                      child: Icon(Icons.skip_previous,
                          color: Colors.lightBlue[600], size: 55),
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        musicController.changeTrack(false);
                      },
                    ),
                    GestureDetector(
                      child: Icon(
                          musicController.isPlaying
                              ? Icons.pause_circle_filled_rounded
                              : Icons.play_circle_fill_rounded,
                          color: Colors.lightBlue[600],
                          size: 65),
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        musicController.changeStatus();
                      },
                    ),
                    GestureDetector(
                      child: Icon(Icons.skip_next,
                          color: Colors.lightBlue[600], size: 55),
                      behavior: HitTestBehavior.translucent,
                      onTap: () {
                        musicController.changeTrack(true);
                      },
                    ),
                  ],
                ),
              ),
            ]),
          ),
        ));
  }
}
