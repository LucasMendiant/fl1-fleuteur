import 'package:flutter/material.dart';
// import 'package:multimedia_app/screens/musicplayer/track_listener.dart';
import 'package:multimedia_app/widgets/nav-drawer.dart';
import 'dart:io';
import 'package:provider/provider.dart';
import 'package:multimedia_app/controller/musicController.dart';

class Musics extends StatefulWidget {
  @override
  _MusicPlayer createState() => _MusicPlayer();
}

class _MusicPlayer extends State<Musics> {
Widget build(BuildContext context) {
    MusicController musicController = Provider.of<MusicController>(context);
    musicController.getTracks();
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text('Médiathèque'),
        backgroundColor: Colors.lightBlue[600],
      ),
      body: Container(
        child: ListView.separated(
          separatorBuilder: (context, index) => Divider(),
          itemCount: musicController.songs.length,
          itemBuilder: (context, index) => ListTile(
            leading: CircleAvatar(
              backgroundImage: musicController.songs[index].albumArtwork == null
                  ? AssetImage('assets/music_background.jpg')
                  : FileImage(File(musicController.songs[index].albumArtwork)),
            ),
            title: Text(musicController.songs[index].title),
            subtitle: Text(musicController.songs[index].artist),
            onTap: () {
              // musicController.setCurrentIndex(index);
              // musicController.setSong(musicController.songs[musicController.currentIndex]);
              // Navigator.of(context).push(MaterialPageRoute(
              //     builder: (context) => TrackListener()));
            },
          ),
        ),
      ),
    );
  }
}
