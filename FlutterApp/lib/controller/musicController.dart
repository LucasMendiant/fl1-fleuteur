import 'package:flutter/material.dart';
import 'package:flutter_audio_query/flutter_audio_query.dart';
import 'package:just_audio/just_audio.dart';

class MusicController with ChangeNotifier {
  FlutterAudioQuery _audioQuery = FlutterAudioQuery();
  List<SongInfo> _songs = [];
  int _currentIndex = 0;
  bool _isNext;

  double _minimumValue = 0.0, _maximumValue = 0.0, _currentValue = 0.0;
  String _currentTime = '', _endTime = '';
  bool _isPlaying = false;
  AudioPlayer _player = AudioPlayer();

  SongInfo _songInfo;

  FlutterAudioQuery get audioQuery => _audioQuery;
  List<SongInfo> get songs => _songs;
  int get currentIndex => _currentIndex;
  bool get isNext => _isNext;
  double get minimumValue => _minimumValue;
  double get maximumValue => _maximumValue;
  double get currentValue => _currentValue;
  String get currentTime => _currentTime;
  String get endTime => _endTime;
  bool get isPlaying => _isPlaying;
  AudioPlayer get player => _player;
  SongInfo get songInfo => _songInfo;


  void getTracks() async {
    _songs = await _audioQuery.getSongs();
    notifyListeners();
  }

  void changeTrack(bool isNext) {
    if (isNext) {
      if (_currentIndex != _songs.length - 1) {
        _currentIndex++;
      }
    }
    else {
      if (_currentIndex != 0) {
        _currentIndex--;
      }
    }
    setSong(_songs[_currentIndex]);
    notifyListeners();
  }

  @override
  void dispose() {
    super.dispose();
    _player?.dispose();
  }

  void setSong(SongInfo songInfo) async {
    _songInfo = songInfo;
    await _player.setUrl(_songInfo.uri);
    _currentValue = _minimumValue;
    _maximumValue = _player.duration.inMilliseconds.toDouble();
    _currentTime = getDuration(_currentValue);
    _endTime = getDuration(_maximumValue);
    _isPlaying = false;
    changeStatus();
    _player.positionStream.listen((duration) {
      _currentValue = duration.inMilliseconds.toDouble();
      _currentTime = getDuration(_currentValue);
      if (_currentValue == _maximumValue) {
        changeTrack(true);
      }
    });
    notifyListeners();
  }

  void changeStatus() {
    _isPlaying = !_isPlaying;
    if (_isPlaying) {
      _player.play();
    } 
    else {
      _player.pause();
    }
    notifyListeners();
  }

  void stop() {
    _player.stop();
    notifyListeners();
  }

  String getDuration(double value) {
    Duration duration = Duration(milliseconds: value.round());

    return [duration.inMinutes, duration.inSeconds]
        .map((element) => element.remainder(60).toString().padLeft(2, '0'))
        .join(':');
  }

  void setCurrentIndex(int value) {
    _currentIndex = value;
    notifyListeners();
  }

  void setCurrentValue(double value) {
    _currentValue = value;
    notifyListeners();
  }

}