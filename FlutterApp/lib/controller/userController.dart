import 'package:flutter/widgets.dart';
import 'package:localstorage/localstorage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multimedia_app/models/data-store-classes-profile.dart';

class UserController with ChangeNotifier {
  TextEditingController _bioController = TextEditingController();
  TextEditingController _ageController = TextEditingController();
  TextEditingController _townController = TextEditingController();
  TextEditingController _facebookUrlController = TextEditingController();
  TextEditingController _instaUrlController = TextEditingController();
  TextEditingController _twitterUrlController = TextEditingController();
  final LocalStorage storage = new LocalStorage('localStorage_app');
  String _countrySelected = '';
  List _countriesList = new CountryList().countriesList;
  String _currentItemSelected = 'Changer votre pays';

  final ImagePicker _picker = ImagePicker();
  String _imageUrl;
  String _email;
  String _bio;
  String _age;
  String _town;
  String _country;
  String _facebookUrl;
  String _instaUrl;
  String _twitterUrl;
  

  TextEditingController get bioController => _bioController;
  TextEditingController get ageController => _ageController;
  TextEditingController get townController => _townController;
  TextEditingController get facebookUrlController => _facebookUrlController;
  TextEditingController get instaUrlController => _instaUrlController;
  TextEditingController get twitterUrlController => _twitterUrlController;
  String get countrySelected => _countrySelected;
  List get countriesList => _countriesList;
  String get currentItemSelected => _currentItemSelected;
  
  String get imageUrl => _imageUrl;
  String get email => _email;
  String get bio => _bio;
  String get age => _age;
  String get town => _town;
  String get country => _country;
  String get facebookUrl => _facebookUrl;
  String get instaUrl => _instaUrl;
  String get twitterUrl => _twitterUrl;
  
  initializerImageUrl() {
    _imageUrl = storage.getItem('imageUrl');
  }

  initializerBio() {
    _bio = storage.getItem('bio');
  }

  initializerEmail() {
    var user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      _email = user.email;
    }
    else {
      _email = "unknow";
    }
  }

  initializerAge() {
    _age = storage.getItem('age');
  }

  initializerTown() {
    _town = storage.getItem('town');
  }

  initializerCountry() {
    _country = storage.getItem('country');
  }

  initializerFacebook() {
    _facebookUrl = storage.getItem('facebookUrl');
  }

  initializerInsta() {
    _instaUrl = storage.getItem('instaUrl');
  }

  initializerTwitter() {
    _twitterUrl = storage.getItem('twitterUrl');
  }

  void initializerAllVariable() {
    initializerImageUrl();
    initializerEmail();
    initializerBio();
    initializerAge();
    initializerTown();
    initializerCountry();
    initializerFacebook();
    initializerInsta();
    initializerTwitter();
  }
  

  void refresh(String country) {
    _countrySelected = country;
    notifyListeners();
  }

  void takePicture(ImageSource source) async {
    final PickedFile pickedFile = await _picker.getImage(
      source: source,
    );
    setImageUrl(pickedFile.path);
  }

  void setImageUrl(String value) {
    _imageUrl = value;
    storage.setItem('imageUrl', value);
    notifyListeners();
  }

  void setBio(String value) {
    _bio = value;
    storage.setItem('bio', value);
    notifyListeners();
  }

  void setAge(String value) {
    _age = value;
    storage.setItem('age', value);
    notifyListeners();
  }

  void setTown(String value) {
    _town = value;
    storage.setItem('town', value);
    notifyListeners();
  }

  void setCountry(String value) {
    _country = value;
    storage.setItem('country', value);
    notifyListeners();
  }

  void setFacebookUrl(String value) {
    _facebookUrl = value;
    storage.setItem('facebookUrl', value);
    notifyListeners();
  }

  void setInstaUrl(String value) {
    _instaUrl = value;
    storage.setItem('instaUrl', value);
    notifyListeners();
  }

  void setTwitterUrl(String value) {
    _twitterUrl = value;
    storage.setItem('twitterUrl', value);
    notifyListeners();
  }

}
