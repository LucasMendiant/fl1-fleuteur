import 'package:flutter/material.dart';
import 'package:multimedia_app/screens/profile/profile.dart';
import 'package:multimedia_app/screens/gallery/gallery.dart';
import 'package:multimedia_app/screens/musicplayer/musicplayer.dart';
import 'package:multimedia_app/services/authservices.dart';
import '../../main.dart';

class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.home_outlined),
              title: Text('Acceuil'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MyHomePage()));
              },
            ),
            ListTile(
              leading: Icon(Icons.account_circle_outlined),
              title: Text('Profile'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => InitScreen()));
              },
            ),
            ListTile(
              leading: Icon(Icons.insert_photo_outlined),
              title: Text('Galerie'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Gallery()));
              },
            ),
            ListTile(
              leading: Icon(Icons.play_circle_outline_rounded),
              title: Text('Médiathèque'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Musics()));
              },
            ),
            ListTile(
              title: Text('Se déconnecter'),
              onTap: () {
                AuthService().signOut();
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
