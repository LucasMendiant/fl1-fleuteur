import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:multimedia_app/controller/userController.dart';

class SocialNetworksTiles extends StatelessWidget {
  @override
  Widget build (BuildContext context) {
    UserController userController = Provider.of<UserController>(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ListTile(
            leading: Icon(Icons.chat),
            title: Text(
              'Facebook :',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.blueAccent
              ),
            ),
            subtitle: Text(userController.facebookUrl == null ? 'Entrer votre url Facebook' : userController.facebookUrl)
        ),
        ListTile(
            leading: Icon(Icons.favorite),
            title: Text(
              'Instagram :',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.blueAccent
              ),
            ),
            subtitle: Text(userController.instaUrl == null ? 'Entrer votre url insta' : userController.instaUrl)
        ),
        ListTile(
            leading: Icon(Icons.contactless_rounded),
            title: Text(
              'Twitter :',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.blueAccent
              ),
            ),
            subtitle: Text(userController.twitterUrl == null ? 'Entrer votre urltwitter' : userController.twitterUrl)
        )
      ],
    );
  }
}