import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:multimedia_app/controller/userController.dart';

class ProfileTiles extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    UserController userController = Provider.of<UserController>(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ListTile(
            leading: Icon(Icons.assignment_ind),
            title: Text(
              'Votre age :',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.blueAccent
              ),
            ),
            subtitle: Text(userController.age == null ? 'Entrer votre age' : userController.age)
        ),
        ListTile(
            leading: Icon(Icons.add_location_alt_rounded),
            title: Text(
              'Votre ville de naissance :',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.blueAccent
              ),
            ),
            subtitle: Text(userController.town == null ? 'Entrer votre ville de naissance' : userController.town)
        ),
        ListTile(
            leading: Icon(Icons.airplanemode_active_rounded),
            title: Text(
              'Votre pays :',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.blueAccent
              ),
            ),
            subtitle: Text(userController.country == null ? 'Entrer votre pays' : userController.country)
        )
      ],
    );
  }
}