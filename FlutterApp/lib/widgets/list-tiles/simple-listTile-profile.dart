import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:multimedia_app/controller/userController.dart';

class SimpleTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    UserController userController = Provider.of<UserController>(context);
    return ListTile(
      leading: Icon(Icons.account_circle),
      title: Text(
        'Votre Bio :',
        style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.blueAccent
        ),
      ),
      subtitle: Text(
        userController.bio == null ? 'Entrer une nouvelle bio' : userController.bio,
        style: TextStyle(fontWeight: FontWeight.normal),
      ),
    );
  }
}