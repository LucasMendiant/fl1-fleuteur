import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DropDownButtonCountry extends StatefulWidget {
  final Function countryCallback;
  final List<String> countries;
  final String currentItemSelected;
  final String country;

  DropDownButtonCountry({Key key, this.countries, this.currentItemSelected, this.country, this.countryCallback}) : super(key: key);

  @override
  DropDownButtonCountryState createState() => DropDownButtonCountryState();
}

class DropDownButtonCountryState extends State<DropDownButtonCountry> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(12.0),
      child: DropdownButtonFormField(
        decoration: InputDecoration(
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: 'Changer votre pays',
            border: OutlineInputBorder(),
            prefixIcon: Icon(Icons.airplanemode_active_rounded)
        ),
        icon: Icon(
            Icons.arrow_drop_down
        ),
        iconSize: 36.0,
        items: widget.countries.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Text(
              dropDownStringItem,
              style: TextStyle(
                  color: Color(0xFF656565)
              ),
            ),
          );
        }).toList(),
        onChanged: (String newValueSelected) {
          setState(() {
            widget.countryCallback(newValueSelected);
          });
        },
      ),
    );
  }
}