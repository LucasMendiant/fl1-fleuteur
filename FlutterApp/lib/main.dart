import 'package:flutter/material.dart';
import 'package:multimedia_app/services/authservices.dart';
import 'package:multimedia_app/widgets/nav-drawer.dart';
import 'package:provider/provider.dart';
import 'package:multimedia_app/controller/userController.dart';
import 'package:multimedia_app/controller/musicController.dart';
import 'package:firebase_core/firebase_core.dart';

void main () async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => UserController()),
        ChangeNotifierProvider(create: (context) => MusicController()),
      ],
      child: MyApp(),
    ),
    );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter multimedia app',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: AuthService().handleAuth(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text('Application multimedia'),
      ),
      body: Center(
        child: Text('Bienvenue sur ton application multimedia.\nChoisi entre profile, galerie et musique grâce au menu de gauche.\n',
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.black.withOpacity(0.6),
        height: 2, fontSize: 20),),
      ),
    );
  }
}