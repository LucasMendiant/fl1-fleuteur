# FlutterApp

FlutterApp est une application mobile développée dans le cadre des projets Epitech de 2021.
__Projet développé par Godeffroy Roué, Justin Vanier et Lucas Mendiant.__

## Installation

__Etape 1:__

Téléchargez ou clonez le repo en utilisant le lien ci-dessous:


```bash
git clone https://gitlab.com/LucasMendiant/fl1-fleuteur.git
```
__Etape 2:__

Allez à la base du projet et éxecutez la commande suivante dans la console afin de charger les dépendences nécéssaires au projet:

```bash
flutter pub get 
```
__Etape 3:__

Afin de lancer l'application en mode debug vous pouvez lancer la commande suivante:

```bash
flutter run 
```
__Etape 4:__

__Génération d'une app pour Android:__

```bash
flutter build apk --release
```
__Génération d'une app pour Ios: (ne fonctionne pas)__


```bash
flutter build ios
```

## Flutter App

Flutter app est une application gallerie permettant de visualiser les photos et musiques de son téléphone.  

Le projet permet également un système d'authentification (firebase) avec le moyen de s'enregistrer, de se connecter ou de se déconnecter de l'application.  

Un menu permet une navigation simple et rapide à travers l'application mais également d'afficher le profile de l'utilisateur connecté.

L'image de profile de l'utilisateur peut être modifié grâce à image photo du téléphone ou de part la caméra.

L'ensemble des données sont gérés par Provider.

## Contribution
Godeffroy Roué, Lucas Mendiant, Justin Vanier

## Licence
Projet open source :)
